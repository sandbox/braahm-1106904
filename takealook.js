
(function ($) {
   
  Drupal.behaviors.takealookFieldsetSummeries = {
    attach: function (context) {
      $('fieldset.takealook-settings-form', context).drupalSetSummary(function (context) {
        if($('.form-item-takealook-enabled input', context).is(':checked')) {

          var threshold = $('.form-item-takealook-threshold input', context).val();
          var window = $('.form-item-takealook-window input', context).val()/60;
          return Drupal.t('Flooding enabled: @threshold time(s) in @window minute(s)', {'@threshold' : threshold, '@window' : window});
        }
        else {
          return Drupal.t('Flooding disabled');
        }
        
      }); 
    }
  }
})(jQuery);