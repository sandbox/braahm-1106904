Goal: restrict access to content based upon previous seeing the content

1. admin screens
        * enable/disable and defaults per content type
        * settings per node
2. access logic
        * per node
        * resctriced on:
                * uid
                * cooky

                        
Flow:
1. test if restriction to content is possible on a normal node
        * hook_node_view to add information on node view => possible to execute code too? ok
        
        * hook_node_access => node + user ok
        
        * hook_node_view_alter

2. build admin panel on node-types page


ToDo:
* admin fields on a per node basis
* refine: pass username, cooky or ipadress
* set a message to the user saying when they can view the node and/or redirect
* change the variables names when the machine readable node type has changed

NEXT STEPS:
1. submit function that stores the settings in the tb
2. add a form on each node page to set the settings
3. change the permissions on the page




ToDo:

Make a patch for the examples node_api module => cleaning up variables

hook_node_insert
=> the values of the form fields automatically become part of the node object
==> use it to insert the values in the db
==> only executed for new nodes (hook node update for existing nodes)

extra:
hook_node_load
=> get the values out of the db and add it to the node object

hook_node_update
=> called when node is updated (insert isn't called anymore)
==> use db_delete en db_insert instead of update => update: if node already existing and module gets enabled later => impossible to update the existing nodes because nothing in yout custom table
** deleting a row that doesn't exist doesn't throw an error


hook_node_delete
=> to delete our stuff from the db


toDo:
* blogpost ;-)

* could expand the permission system with create, update, disable rights on nodes
takealook_set_flooding_on_articles

